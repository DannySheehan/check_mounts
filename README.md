# check_mounts.sh

Checks the mount points configured in _/etc/fstab_

- that they are actually all mounted 
- that none of them have become read only (ro) instead of read write (rw). 
    - this can happen in some SAN environment
- keeps a log of problems in _/tmp/check_mounts.log_

Only tested on for Linux (CentOS)